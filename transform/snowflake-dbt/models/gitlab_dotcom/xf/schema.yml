version: 2

models:
  - name: gitlab_dotcom_gitlab_issues_requests
    description: '{{ doc("gitlab_dotcom_gitlab_issues_requests") }}'
    columns:
      - name: issue_id
        tests:
          - not_null
      - name: project_id
        tests:
          - not_null
      - name: namespace_id
        tests:
          - not_null
      - name: sfdc_account_id
        tests:
          - not_null

  - name: gitlab_dotcom_internal_notes_xf
    description: '{{ doc("gitlab_dotcom_internal_notes_xf") }}'
    columns:
      - name: note_id
        tests:
          - not_null
          - unique
      - name: created_at
        tests:
          - not_null
      - name: updated_at
        tests:
          - not_null
      - name: noteable_type
        tests:
          - accepted_values:
                      values: ['Commit', 'Epic', 'Issue', 'MergeRequest', 'Snippet', 'Wiki', 'DesignManagement::Design']

  - name: gitlab_dotcom_group_audit_events_monthly
    description: '{{ doc("gitlab_dotcom_group_audit_events_monthly") }}'
    columns:
      - name: group_id
        tests:
          - not_null
      - name: audit_events_count
        description: The number of audit_events captured for the user during that month.
        tests:
          - not_null
      - name: months_since_creation_date
        description: Reflects the number of months between when a user joined and the month of the audit event(s).
        tests:
          - not_null

  - name: gitlab_dotcom_issues_xf
    description: '{{ doc("gitlab_dotcom_issues_xf") }}'
    columns:
      - name: issue_id
        tests:
          - not_null
      - name: issue_title
        description: '{{ doc("xf_visibility_documentation") }}'
      - name: issue_description
        description: '{{ doc("xf_visibility_documentation") }}'

  - name: gitlab_dotcom_label_states_xf
    description: Represents the latest state (added or removed) for each label on Epics/Issues/Merge Requests
    columns:
      - name: label_id
        tests:
          - not_null


  - name: gitlab_dotcom_labels_xf
    description: '{{ doc("gitlab_dotcom_labels_xf") }}'
    columns:
      - name: label_id
        tests:
          - not_null
      - name: masked_label_title
        description: '{{ doc("xf_visibility_documentation") }}'

  - name: gitlab_dotcom_groups_xf
    description: '{{ doc("gitlab_dotcom_groups_xf") }}'
    columns:
      - name: group_id
        tests:
          - not_null
          - unique
      - name: group_plan_is_paid
        description: Whether or not the group is subscribed to a paid plan. A subgroup inherits from the subscription of its ultimate parent group
      - name: member_count
        description: The number of members that are presently associated with the group.
      - name: project_count
        description: The number of projects that are presently associated with the group.

  - name: gitlab_dotcom_events_monthly_active_users
    description: '{{ doc("gitlab_dotcom_events_monthly_active_users") }}'
    columns:
      - name: day
        tests:
          - not_null
          - unique
      - name: is_last_day_of_month
        tests:
          - not_null
      - name: count_audit_events_active_users_last_28_days
        description: The number of unique active users (from audit events table) in the previous 28 days (inclusive). 
      - name: count_audit_events_active_users_last_28_days
        description: The number of unique active users (from events table) in the previous 28 days (inclusive). 
        
  - name: gitlab_dotcom_merge_requests_xf
    description: '{{ doc("gitlab_dotcom_merge_requests_xf") }}'
    columns:
      - name: merge_request_id
        tests:
          - not_null
      - name: is_community_contributor_related
        description: Merge Request has 'community contribution' tag, and is on a project in the gitlab.org namespace.

  - name: gitlab_dotcom_milestones_xf
    description: This is a non-sensitive models of the milestones table. The "title" and "description" columns are masked for non-internal namespaces.
    columns:
      - name: milestone_id
        tests:
          - not_null
          - unique
      - name: milestone_status
        tests:
          - not_null
      - name: milestone_created_at
        tests:
          - not_null
      - name: milestone_updated_at
        tests:
          - not_null
      - name: namespace_id
        description: This is added as a helper column. This is either the group_id (groups are namespaces) or the namespace associated with the milestone's project.


  - name: gitlab_dotcom_namespaces_xf
    description: '{{ doc("gitlab_dotcom_namespaces_xf") }}'
    columns:
      - name: namespace_id
        tests:
          - not_null
          - unique
      - name: namespace_name
        tests:
          - not_null
      - name: namespace_path
        tests:
          - not_null
      - name: namespace_type
        tests:
          - not_null
      - name: plan_id
        description: The plan associated with the namespace's subscription (via gitlab_subscriptions)
        tests:
          - not_null
          - accepted_values:
              values: [2, 3, 4, 34]
      - name: namespace_plan_is_paid
        description: Whether or not the namespace associated with the project is subscribed to a paid plan.
        tests:
          - not_null
      - name: member_count
        description: The number of members that are presently associated with the namespace.
      - name: project_count
        description: The number of projects that are presently associated with the namespace.

  - name: gitlab_dotcom_projects_xf
    description: '{{ doc("gitlab_dotcom_projects_xf") }}'
    columns:
      - name: project_id
        tests:
          - not_null
          - unique
      - name: member_count
        description: The number of members that are presently associated with the project.
      - name: namespace_plan_is_paid
        description: Whether or not the namespace associated with the project is subscribed to a paid plan t.

  - name: gitlab_dotcom_retention_cohorts
    description: '{{ doc("gitlab_dotcom_retention_cohorts") }}'
    columns:
        - name: cohort_key
          description: md5 of cohort date and cohort period in other to provide a unique key
          tests:
            - not_null
            - unique
        - name: cohort_date
          description: Users are cohorted based on the month their account was created on gitlab.com.
          tests:
            - not_null
        - name: period
          description: Length in months of activity period, using the difference between `created_at` and `last_activity_on`.
          tests:
            - not_null
        - name: active_in_period_distinct_count
          description: Count of distinct Users active on this period.
        - name: base_cohort_count
          description: Size of the original cohort.
        - name: retention
          description: Calculated as `active_in_period_distinct_count` / `base_cohort_count`.

  - name: gitlab_dotcom_users_xf
    description: '{{ doc("gitlab_dotcom_users_xf") }}'
    columns:
      - name: user_id
        tests:
          - not_null
          - unique
      - name: days_active
        description: days between user creation and last activity
      - name: account_age_cohort
        description: cohorting of time between last dbt run and user creation date.
